BiglyBT system service
----------------------

BiglyBTd is BiglyBT wrapped into a system service.

It uses the "telnet" or "console" UI and therefore doesn't need a display
to work.

The "telnet" UI permits to interact with BiglyBTd. Through it you can launch
another UI, for example the "swt" UI (which is the one displayed when using
BiglyBT the standard way)

Provided you install and configure the plugin "BiglyBT Web Remote" (see
BigltBT plugin page), you could also interact with BiglyBTd through your
browser.

To access the command line of BiglyBTd through telnet, run:

  $ telnet 127.0.0.1 57006

Available commands will be displayed to you.

To launch the "swt" UI of BiglyBTd, launch:

  $ /usr/bin/biglybtd-gui

So that the "swt" UI gets displayed in your desktop environment, you need to
finish the configuration as below.


Configuration
-------------

 * The biglybtd package:

   - installs the systemd service

   - creates system user "biglybt" whose HOME is located at /var/lib/biglybt.

 * You have in addition to:

   - Edit file /etc/sudoers.d/biglybtd-gui-xauth and set your username on
     the User_Alias line

     # visudo /etc/sudoers.d/biglybtd-gui-xauth

     Rationale: From a technical point of view, displaying the SWT UI on the
     screen of the current user requires the biglybt user to have access to
     your display. The /usr/bin/biglybtd-gui script will do this for you but
     you must have the rights to launch xauth on behalf of biglybt user.

   - If you plan to open the SWT UI of biglybtd, check that DISPLAY environment
     of biglybtd.service is correct. By default biglybtd uses a value of ":0"

     1. Get the value of DISPLAY environment variable when running your desktop
        enviroment:

       $ echo $DISPLAY

     2. Edit /etc/systemd/system/biglybtd.service.d/ and set the value of DISPLAY.

       # editor /etc/systemd/system/biglybtd.service.d/env_display.conf

     3. Finally you need to tell systemd to reload the file:

       # systemctl daemon-reload

   - By default, the deamon is neither enabled, nor started. You may want
     to do so by running:

       # systemctl enable biglybtd.service
       # systemctl start biglybtd.service

 * You may in addition:

   - Add your user to the "biglybt" group:

        $ usermod -a -G biglybt <username>

   - Use the "console" UI instead of "telnet". By default, the package uses
     "telnet" UI. If you don't wish that, you may change this in the
     "/lib/systemd/system/biglybtd.service" file and set it to "console"
     instead. Run "systemctl daemon-reload" after the change.


Limitations, known issues
--------------------------

- If you open the SWT UI of the daemonized BiglyBT, and then quit that UI,
  it will also fully close BiglyBT (the daemonized part also). However,
  systemd will restart it immediately.

- To be able to display the SWT UI of the daemonized BiglyBT in your desktop
  environment, you have to grant the user the right to export his .Xauthority
  MIT-MAGIC-COOKIE-1 to the biglybt user. You have to set your username at
  the top of the sudoers file located here: /etc/sudoers.d/biglybtd-gui-xauth

- The first time you launch /usr/bin/biglybtd-gui, you may have this message:

    /usr/bin/xauth:  file /var/lib/biglybt/.Xauthority does not exist

  This is normal since the file didn't exist. xauth command created it,
  so now it's ok.

- Thanks to the use of "-Dazureus.instance.port=6879" instead of the default
  value (6880), it is possible to have another instance of BiglyBT running
  (for example as a regular user) in addition to the deamonized one. However
  this has not been tested.

- The telnet port can't be configured. However you may check the value of
  Telnet_iPort in /var/lib/biglybt/config/biglybt.config. Some more
  information here:

    https://github.com/BiglySoftware/BiglyBT/blob/master/uis/src/com/biglybt/ui/telnet/UI.java

    http://wiki.vuze.com/w/TelnetUI


Some references:

* https://github.com/BiglySoftware/BiglyBT/issues/356
