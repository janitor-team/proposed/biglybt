#!/bin/sh

JAVA_ARGS=$(grep -o '^[^#]*' ~/.biglybt/java.vmoptions 2>/dev/null | tr '\n' ' ')

# if command is invoked as "biglybtd", then set the config path
# to the one for the user defined in the systemd service
if [ "$0" = "/usr/bin/biglybtd" ]; then
  BIGLYBTD_USER=$(systemctl show -p User --value biglybtd.service)
  BIGLYBTD_USER_HOME=$( getent passwd "$BIGLYBTD_USER" | cut -d: -f6 )
  BIGLYBTD_USER_CONFIG_PATH=$BIGLYBTD_USER_HOME/config

  PARAMS=" -Dazureus.config.path=$BIGLYBTD_USER_CONFIG_PATH -Dazureus.instance.port=6879 "
  if [ ! "$USER" = "$BIGLYBTD_USER" ]; then
    echo "only $BIGLYBTD_USER is allowed to run as daemon"
    exit
  fi
fi

java -cp /usr/share/java/biglybt-core.jar:/usr/share/java/biglybt-ui.jar:/usr/share/java/commons-cli.jar:/usr/share/java/swt4.jar:/usr/share/java/bcprov.jar \
     ${JAVA_ARGS} \
     ${PARAMS} \
     -Dazureus.install.path=/usr/share/biglybt \
     com.biglybt.ui.Main "$@"
